-- AstroCore provides a central place to modify mappings, vim options, autocommands, and more!
-- Configuration documentation can be found with `:h astrocore`
-- NOTE: We highly recommend setting up the Lua Language Server (`:LspInstall lua_ls`)
--       as this provides autocomplete and documentation while editing

local function create_autocmds(pattern, command)
  return {
    {
      event = "BufNewFile",
      pattern = pattern,
      command = command
    },
    {
      event = "BufRead",
      pattern = pattern,
      command = command
    }
  }
end

---@type LazySpec
return {
  "AstroNvim/astrocore",
  ---@type AstroCoreOpts
  opts = {
    -- Autocommands
    autocmds = {
      tplishtml = create_autocmds("*.tpl", "set filetype=html"),
      confisxml = create_autocmds("*/hinetx/*.conf", "set filetype=xml"),
    },
    -- Configure core features of AstroNvim
    features = {
      large_buf = { size = 1024 * 5000, lines = 20000 }, -- set global limits for large files for disabling features like treesitter
      autopairs = true,                                  -- enable autopairs at start
      cmp = true,                                        -- enable completion at start
      diagnostics_mode = 3,                              -- diagnostic mode on start (0 = off, 1 = no signs/virtual text, 2 = no virtual text, 3 = on)
      highlighturl = true,                               -- highlight URLs at start
      notifications = true,                              -- enable notifications at start
    },
    -- Diagnostics configuration (for vim.diagnostics.config({...})) when diagnostics are on
    diagnostics = {
      virtual_text = true,
      underline = true,
    },
    -- vim options can be configured here
    options = {
      opt = {                   -- vim.opt.<key>
        title = false,
        relativenumber = false, -- sets vim.opt.relativenumber
        number = true,          -- sets vim.opt.number
        spell = false,          -- sets vim.opt.spell
        signcolumn = "auto",    -- sets vim.opt.signcolumn to auto
        wrap = true,            -- sets vim.opt.wrap
        clipboard = "unnamedplus",
        scrolloff = 10
      },
      g = { -- vim.g.<key>
        -- configure global vim variables (vim.g)
        -- NOTE: `mapleader` and `maplocalleader` must be set in the AstroNvim opts or before `lazy.setup`
        -- This can be found in the `lua/lazy_setup.lua` file
      },
      o = {
        guifont = "JetBrainsMono Nerd Font:h9"
      }
    },
    -- Mappings can be configured through AstroCore as well.
    -- NOTE: keycodes follow the casing in the vimdocs. For example, `<Leader>` must be capitalized
    mappings = {
      o = {
        -- nvim spider keymap
        ["w"] = {
          function() require("spider").motion("w") end,
          desc = "Spider-w"
        },
        ["e"] = {
          function() require("spider").motion("e") end,
          desc = "Spider-e"
        },
        ["b"] = {
          function() require("spider").motion("b") end,
          desc = "Spider-b"
        },
        ["ge"] = {
          function() require("spider").motion("ge") end,
          desc = "Spider-ge"
        },

      },
      x = {
        -- nvim spider keymap
        ["w"] = {
          function() require("spider").motion("w") end,
          desc = "Spider-w"
        },
        ["e"] = {
          function() require("spider").motion("e") end,
          desc = "Spider-e"
        },
        ["b"] = {
          function() require("spider").motion("b") end,
          desc = "Spider-b"
        },
        ["ge"] = {
          function() require("spider").motion("ge") end,
          desc = "Spider-ge"
        },

      },
      -- first key is the mode
      n = {
        -- second key is the lefthand side of the map

        -- Jumplist
        ["<C-l>"] = {
          "<C-i>"
        },
        ["<C-j>"] = {
          "<C-o>"
        },

        -- nvim spider keymap
        ["w"] = {
          function() require("spider").motion("w") end,
          desc = "Spider-w"
        },
        ["e"] = {
          function() require("spider").motion("e") end,
          desc = "Spider-e"
        },
        ["b"] = {
          function() require("spider").motion("b") end,
          desc = "Spider-b"
        },
        ["ge"] = {
          function() require("spider").motion("ge") end,
          desc = "Spider-ge"
        },

        -- navigate buffer tabs with `H` and `L`
        ["<M-l>"] = {
          function() require("astrocore.buffer").nav(vim.v.count > 0 and vim.v.count or 1) end,
          desc = "Next buffer",
        },
        ["<M-h>"] = {
          function() require("astrocore.buffer").nav(-(vim.v.count > 0 and vim.v.count or 1)) end,
          desc = "Previous buffer",
        },

        -- Better Neotre actions
        ["<Leader>e"] = {
          "<cmd>Neotree toggle last<cr>",
          desc = "Toggle Explorer"
        },
        ["<Leader>o"] = {
          function()
            if vim.bo.filetype == "neo-tree" then
              vim.cmd.wincmd "p"
            else
              vim.cmd.Neotree("focus","last")
            end
          end,
          desc = "Toggle Explorer Focus"
        },

        -- mappings seen under group name "Buffer"
        ["<Leader>bD"] = {
          function()
            require("astroui.status.heirline").buffer_picker(
              function(bufnr) require("astrocore.buffer").close(bufnr) end
            )
          end,
          desc = "Pick to close",
        },
        -- tables with just a `desc` key will be registered with which-key if it's installed
        -- this is useful for naming menus
        ["<Leader>b"] = { desc = "󰌨 Buffers" },
        ["<Leader>i"] = { desc = "󰅲 Surround" },

        -- bookmarks
        ["<Leader>'"] = { desc = "  Bookmarks" },
        ["<Leader>'b"] = {
          "<cmd>BookmarksCommands<cr>",
          desc = "Commands"
        },
        ["<Leader>'l"] = {
          "<cmd>BookmarksMarkGotoBookmarkInList<cr>",
          desc = "Go to bookmark in list"
        },

        -- quick save
        -- ["<C-s>"] = { ":w!<cr>", desc = "Save File" },  -- change description but the same command

        -- AstroNvim actions
        ["<Leader>a"] = { desc = " AstroNvim" },
        ["<Leader>au"] = {
          "<cmd>AstroUpdate<cr>",
          desc = "Update AstroNvim"
        },
        ["<Leader>ar"] = {
          "<cmd>AstroReload<cr>",
          desc = "Reload config files"
        },

        -- AstroNvim actions
        ["<Leader>C"] = { desc = " Copilot" },
        ["<Leader>Cc"] = {
          "<cmd>CopilotChat<cr>",
          desc = "Copilot Chat"
        },

        -- More gitsign actions
        ["<Leader>ge"] = {
          function()
            require 'gitsigns'.next_hunk()
          end,
          desc = "Next hunk"
        },
        ["<Leader>gi"] = {
          function()
            require 'gitsigns'.prev_hunk()
          end,
          desc = "Prev hunk"
        },
        ["<Leader>gr"] = {
          function()
            require 'gitsigns'.reset_hunk()
          end,
          desc = "Reset hunk"
        },
        ["<Leader>gR"] = {
          function()
            require 'gitsigns'.reset_buffer()
          end,
          desc = "Reset buffer"
        },

        -- Colemak focus on split buffers
        ["<C-n>"] = { function() require("smart-splits").move_cursor_left() end, desc = "Move to left split" },
        ["<C-e>"] = { function() require("smart-splits").move_cursor_down() end, desc = "Move to below split" },
        ["<C-i>"] = { function() require("smart-splits").move_cursor_up() end, desc = "Move to above split" },
        ["<C-o>"] = { function() require("smart-splits").move_cursor_right() end, desc = "Move to right split" },

        -- Utils
        ["<Enter>"] = { function() return vim.bo.modifiable and "o<Esc>" or "<CR>" end, expr = true, desc = "Insert line below" },
        ["<A-Enter>"] = { function() return vim.bo.modifiable and "O<Esc>" or "<A-Enter>" end, expr = true, desc = "Insert line above" },

        -- Git
        ["<Leader>gg"] = {"<cmd>LazyGit<CR>", desc = "LazyGit"},
        ["<Leader>gh"] = {"<cmd>LazyGitFilterCurrentFile<CR>", desc = "File history"},

        -- Terminal
        ["<Leader>tt"] = { function() require("astrocore").toggle_term_cmd "btop" end, desc = "ToggleTerm btop" }

        -- Desabilitados
        -- ["<Leader>gh"] = false,
      },
      t = {
        -- setting a mapping to false will disable it
        -- ["<esc>"] = false,
      },
    },
  },
}
