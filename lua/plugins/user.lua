-- You can also add or configure plugins by creating files in this `plugins/` folder
-- Here are some examples:

---@type LazySpec
return {
  {
    "rebelot/kanagawa.nvim",
    config = function()
      require("kanagawa").setup {
        dimInactive = true,
        globalStatus = true,
        theme = "wave",
        background = {
          dark = "wave",
          light = "lotus",
        },
        colors = {
          theme = {
            all = {
              ui = {
                bg_gutter = "none",
                bg_visual = "#2D4F67",
              },
            },
          },
        },
      }
    end,
  },

  -- == Examples of Adding Plugins ==

  "andweeb/presence.nvim",
  {
    "ray-x/lsp_signature.nvim",
    event = "BufRead",
    config = function() require("lsp_signature").setup() end,
  },

  -- == Examples of Overriding Plugins ==

  -- customize alpha options
  {
    "goolord/alpha-nvim",
    opts = function(_, opts)
      -- customize the dashboard header
      Headers = {
        {
          "          ▀████▀▄▄              ▄█ ",
          "            █▀    ▀▀▄▄▄▄▄    ▄▄▀▀█ ",
          "    ▄        █          ▀▀▀▀▄  ▄▀  ",
          "   ▄▀ ▀▄      ▀▄              ▀▄▀  ",
          "  ▄▀    █     █▀   ▄█▀▄      ▄█    ",
          "  ▀▄     ▀▄  █     ▀██▀     ██▄█   ",
          "   ▀▄    ▄▀ █   ▄██▄   ▄  ▄  ▀▀ █  ",
          "    █  ▄▀  █    ▀██▀    ▀▀ ▀▀  ▄▀  ",
          "   █   █  █      ▄▄           ▄▀   ",
        },

        {
          "                                                     ",
          "  ███╗   ██╗███████╗ ██████╗ ██╗   ██╗██╗███╗   ███╗ ",
          "  ████╗  ██║██╔════╝██╔═══██╗██║   ██║██║████╗ ████║ ",
          "  ██╔██╗ ██║█████╗  ██║   ██║██║   ██║██║██╔████╔██║ ",
          "  ██║╚██╗██║██╔══╝  ██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║ ",
          "  ██║ ╚████║███████╗╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║ ",
          "  ╚═╝  ╚═══╝╚══════╝ ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝ ",
          "                                                     ",
        },

        {
          [[┌┐┌┬─┐┌─┐┬  ┬┬┌┬┐]],
          [[│││├┤ │ │└┐┌┘││││]],
          [[┘└┘└─┘└─┘ └┘ ┴┴ ┴]],
        },

        {
          [[╔╗╔╦═╗╔═╗╦  ╦╦╔╦╗]],
          [[║║║╠╣ ║ ║╚╗╔╝║║║║]],
          [[╝╚╝╚═╝╚═╝ ╚╝ ╩╩ ╩]],
        },

        {
          [[                               __                ]],
          [[  ___     ___    ___   __  __ /\_\    ___ ___    ]],
          [[ / _ `\  / __`\ / __`\/\ \/\ \\/\ \  / __` __`\  ]],
          [[/\ \/\ \/\  __//\ \_\ \ \ \_/ |\ \ \/\ \/\ \/\ \ ]],
          [[\ \_\ \_\ \____\ \____/\ \___/  \ \_\ \_\ \_\ \_\]],
          [[ \/_/\/_/\/____/\/___/  \/__/    \/_/\/_/\/_/\/_/]],
        },

        {
          "            :h-                                  Nhy`               ",
          "           -mh.                           h.    `Ndho               ",
          "           hmh+                          oNm.   oNdhh               ",
          "          `Nmhd`                        /NNmd  /NNhhd               ",
          "          -NNhhy                      `hMNmmm`+NNdhhh               ",
          "          .NNmhhs              ```....`..-:/./mNdhhh+               ",
          "           mNNdhhh-     `.-::///+++////++//:--.`-/sd`               ",
          "           oNNNdhhdo..://++//++++++/+++//++///++/-.`                ",
          "      y.   `mNNNmhhhdy+/++++//+/////++//+++///++////-` `/oos:       ",
          " .    Nmy:  :NNNNmhhhhdy+/++/+++///:.....--:////+++///:.`:s+        ",
          " h-   dNmNmy oNNNNNdhhhhy:/+/+++/-         ---:/+++//++//.`         ",
          " hd+` -NNNy`./dNNNNNhhhh+-://///    -+oo:`  ::-:+////++///:`        ",
          " /Nmhs+oss-:++/dNNNmhho:--::///    /mmmmmo  ../-///++///////.       ",
          "  oNNdhhhhhhhs//osso/:---:::///    /yyyyso  ..o+-//////////:/.      ",
          "   /mNNNmdhhhh/://+///::://////     -:::- ..+sy+:////////::/:/.     ",
          "     /hNNNdhhs--:/+++////++/////.      ..-/yhhs-/////////::/::/`    ",
          "       .ooo+/-::::/+///////++++//-/ossyyhhhhs/:///////:::/::::/:    ",
          "       -///:::::::////++///+++/////:/+ooo+/::///////.::://::---+`   ",
          "       /////+//++++/////+////-..//////////::-:::--`.:///:---:::/:   ",
          "       //+++//++++++////+++///::--                 .::::-------::   ",
          "       :/++++///////////++++//////.                -:/:----::../-   ",
          "       -/++++//++///+//////////////               .::::---:::-.+`   ",
          "       `////////////////////////////:.            --::-----...-/    ",
          "        -///://////////////////////::::-..      :-:-:-..-::.`.+`    ",
          "         :/://///:///::://::://::::::/:::::::-:---::-.-....``/- -   ",
          "           ::::://::://::::::::::::::----------..-:....`.../- -+oo/ ",
          "            -/:::-:::::---://:-::-::::----::---.-.......`-/.      ``",
          "           s-`::--:::------:////----:---.-:::...-.....`./:          ",
          "          yMNy.`::-.--::..-dmmhhhs-..-.-.......`.....-/:`           ",
          "         oMNNNh. `-::--...:NNNdhhh/.--.`..``.......:/-              ",
          "        :dy+:`      .-::-..NNNhhd+``..`...````.-::-`                ",
          "                        .-:mNdhh:.......--::::-`                    ",
          "                           yNh/..------..`                          ",
          "                                                                    ",
        },

        {
          [[                                         ]],
          [[   ⣴⣶⣤⡤⠦⣤⣀⣤⠆     ⣈⣭⣿⣶⣿⣦⣼⣆            ]],
          [[    ⠉⠻⢿⣿⠿⣿⣿⣶⣦⠤⠄⡠⢾⣿⣿⡿⠋⠉⠉⠻⣿⣿⡛⣦        ]],
          [[          ⠈⢿⣿⣟⠦ ⣾⣿⣿⣷    ⠻⠿⢿⣿⣧⣄        ]],
          [[           ⣸⣿⣿⢧ ⢻⠻⣿⣿⣷⣄⣀⠄⠢⣀⡀⠈⠙⠿⠄      ]],
          [[          ⢠⣿⣿⣿⠈    ⣻⣿⣿⣿⣿⣿⣿⣿⣛⣳⣤⣀⣀     ]],
          [[   ⢠⣧⣶⣥⡤⢄ ⣸⣿⣿⠘  ⢀⣴⣿⣿⡿⠛⣿⣿⣧⠈⢿⠿⠟⠛⠻⠿⠄  ]],
          [[  ⣰⣿⣿⠛⠻⣿⣿⡦⢹⣿⣷   ⢊⣿⣿⡏  ⢸⣿⣿⡇ ⢀⣠⣄⣾⠄    ]],
          [[ ⣠⣿⠿⠛ ⢀⣿⣿⣷⠘⢿⣿⣦⡀ ⢸⢿⣿⣿⣄ ⣸⣿⣿⡇⣪⣿⡿⠿⣿⣷⡄  ]],
          [[ ⠙⠃   ⣼⣿⡟  ⠈⠻⣿⣿⣦⣌⡇⠻⣿⣿⣷⣿⣿⣿ ⣿⣿⡇ ⠛⠻⢷⣄  ]],
          [[      ⢻⣿⣿⣄   ⠈⠻⣿⣿⣿⣷⣿⣿⣿⣿⣿⡟ ⠫⢿⣿⡆       ]],
          [[       ⠻⣿⣿⣿⣿⣶⣶⣾⣿⣿⣿⣿⣿⣿⣿⣿⡟⢀⣀⣤⣾⡿⠃      ]],
          [[                                         ]],
        },
        {
          [[                            ]],
          [[ ⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⣤⣴⣶⣤⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀ ]],
          [[ ⠀⠀⠀⠀⣠⡤⣤⣄⣾⣿⣿⣿⣿⣿⣿⣷⣠⣀⣄⡀⠀⠀⠀⠀]],
          [[ ⠀⠀⠀⠀⠙⠀⠈⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⣬⡿⠀⠀⠀⠀]],
          [[ ⠀⠀⠀⠀⠀⢀⣼⠟⢿⣿⣿⣿⣿⣿⣿⡿⠘⣷⣄⠀⠀⠀⠀⠀]],
          [[ ⣰⠛⠛⣿⢠⣿⠋⠀⠀⢹⠻⣿⣿⡿⢻⠁⠀⠈⢿⣦⠀⠀⠀⠀]],
          [[ ⢈⣵⡾⠋⣿⣯⠀⠀⢀⣼⣷⣿⣿⣶⣷⡀⠀⠀⢸⣿⣀⣀⠀⠀]],
          [[ ⢾⣿⣀⠀⠘⠻⠿⢿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣶⠿⣿⡁⠀⠀⠀]],
          [[ ⠈⠙⠛⠿⠿⠿⢿⣿⡿⣿⣿⡿⢿⣿⣿⣿⣷⣄⠀⠘⢷⣆⠀⠀]],
          [[ ⠀⠀⠀⠀⠀⢠⣿⠏⠀⣿⡏⠀⣼⣿⠛⢿⣿⣿⣆⠀⠀⣿⡇⡀ ]],
          [[ ⠀⠀⠀⠀⢀⣾⡟⠀⠀⣿⣇⠀⢿⣿⡀⠈⣿⡌⠻⠷⠾⠿⣻⠁ ]],
          [[ ⠀⠀⣠⣶⠟⠫⣤⠀⠀⢸⣿⠀⣸⣿⢇⡤⢼⣧⠀⠀⠀⢀⣿⠀ ]],
          [[ ⠀⣾⡏⠀⡀⣠⡟⠀⠀⢀⣿⣾⠟⠁⣿⡄⠀⠻⣷⣤⣤⡾⠋⠀ ]],
          [[ ⠀⠙⠷⠾⠁⠻⣧⣀⣤⣾⣿⠋⠀⠀⢸⣧⠀⠀⠀⠉⠁⠀⠀⠀ ]],
          [[ ⠀⠀⠀⠀⠀⠀⠈⠉⠉⠹⣿⣄⠀⠀⣸⡿⠀⠀⠀⠀⠀⠀⠀⠀  ]],
          [[ ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠙⠛⠿⠟⠛⠁⠀⠀⠀⠀⠀⠀⠀⠀  ]],
          [[                              ]],
        },
        {
          [[                                                ]],
          [[                                          _.oo. ]],
          [[                  _.u[[/;:,.         .odMMMMMM' ]],
          [[               .o888UU[[[/;:-.  .o@P^    MMM^   ]],
          [[              oN88888UU[[[/;::-.        dP^     ]],
          [[             dNMMNN888UU[[[/;:--.   .o@P^       ]],
          [[            ,MMMMMMN888UU[[/;::-. o@^           ]],
          [[            NNMMMNN888UU[[[/~.o@P^              ]],
          [[            888888888UU[[[/o@^-..               ]],
          [[           oI8888UU[[[/o@P^:--..                ]],
          [[        .@^  YUU[[[/o@^;::---..                 ]],
          [[      oMP     ^/o@P^;:::---..                   ]],
          [[   .dMMM    .o@^ ^;::---...                     ]],
          [[  dMMMMMMM@^`       `^^^^                       ]],
          [[ YMMMUP^                                        ]],
          [[  ^^                                            ]],
          [[                                                ]],
        },
        {
          [[                                                   ]],
          [[                                              ___  ]],
          [[                                           ,o88888 ]],
          [[                                        ,o8888888' ]],
          [[                  ,:o:o:oooo.        ,8O88Pd8888"  ]],
          [[              ,.::.::o:ooooOoOoO. ,oO8O8Pd888'"    ]],
          [[            ,.:.::o:ooOoOoOO8O8OOo.8OOPd8O8O"      ]],
          [[           , ..:.::o:ooOoOOOO8OOOOo.FdO8O8"        ]],
          [[          , ..:.::o:ooOoOO8O888O8O,COCOO"          ]],
          [[         , . ..:.::o:ooOoOOOO8OOOOCOCO"            ]],
          [[          . ..:.::o:ooOoOoOO8O8OCCCC"o             ]],
          [[             . ..:.::o:ooooOoCoCCC"o:o             ]],
          [[             . ..:.::o:o:,cooooCo"oo:o:            ]],
          [[          `   . . ..:.:cocoooo"'o:o:::'            ]],
          [[          .`   . ..::ccccoc"'o:o:o:::'             ]],
          [[         :.:.    ,c:cccc"':.:.:.:.:.'              ]],
          [[       ..:.:"'`::::c:"'..:.:.:.:.:.'               ]],
          [[     ...:.'.:.::::"'    . . . . .'                 ]],
          [[    .. . ....:."' `   .  . . ''                    ]],
          [[  . . . ...."'                                     ]],
          [[  .. . ."'                                         ]],
          [[ .                                                 ]],
          [[                                                   ]],
        },
        {
          [[                                ]],
          [[             ,,,,,,             ]],
          [[         o#'9MMHb':'-,o,        ]],
          [[      .oH":HH$' "' ' -*R&o,     ]],
          [[     dMMM*""'`'      .oM"HM?.   ]],
          [[   ,MMM'          "HLbd< ?&H\   ]],
          [[  .:MH ."\          ` MM  MM&b  ]],
          [[ . "*H    -        &MMMMMMMMMH: ]],
          [[ .    dboo        MMMMMMMMMMMM. ]],
          [[ .   dMMMMMMb      *MMMMMMMMMP. ]],
          [[ .    MMMMMMMP        *MMMMMP . ]],
          [[      `#MMMMM           MM6P ,  ]],
          [[  '    `MMMP"           HM*`,   ]],
          [[   '    :MM             .- ,    ]],
          [[    '.   `#?..  .       ..'     ]],
          [[       -.   .         .-        ]],
          [[         ''-.oo,oo.-''          ]],
          [[                                ]],
        },
      }

      local function bibleVerse()
        local handle = io.popen "/home/brenoperes/bin/bible"
        local result = handle:read "*a"
        handle:close()
        local lines = {}
        for line in result:gmatch "[^\r\n]+" do
          table.insert(lines, line)
        end
        return lines
      end

      function combineRandomHeaderWithVerse()
        -- Assuming bibleVerse() is a function that returns a table of verses
        local verses = bibleVerse()

        -- Select a random header from the Headers table
        local header = Headers[math.random(#Headers)]

        -- Create a new table to combine the header and verses
        local combinedTable = {}

        -- Insert the header items into the combined table
        for _, line in ipairs(header) do
          table.insert(combinedTable, line)
        end

        -- Insert the verse items into the combined table
        for _, verse in ipairs(verses) do
          table.insert(combinedTable, verse)
        end

        return combinedTable
      end

      opts.section.header.val = Headers[math.random(#Headers)]
      -- opts.section.header.val = combineRandomHeaderWithVerse()
      -- local width = 54
      -- local height = 12
      local width = 34
      local height = 17
      require "alpha.term"
      -- opts.section.header = {
      --   type = "terminal",
      --   command = "metroid -s",
      --   -- command = "neo -D -m NEOVIM",
      --   -- command = "bible",
      --   width = width,
      --   height = height,
      --   opts = {
      --     position = "center",
      --     redraw = true,
      --   },
      --   window_config = {
      --     height = height,
      --   },
      -- }

      opts.section.buttons.val = {
        opts.button("LDR S .", "  Last Session for dir  "),
        opts.button("LDR n  ", "  New File  "),
        opts.button("LDR f f", "  Find file  "),
        opts.button("LDR f o", "󰈙  Recents  "),
        opts.button("LDR f w", "󰈭  Find word  "),
        opts.button("LDR f a", "  Configuration  "),
        opts.button("LDR ' l", "  Bookmarks  "),
      }
      opts.config.layout = {
        { type = "padding", val = vim.fn.max { 2, vim.fn.floor(vim.fn.winheight(0) * 0.1) } },
        opts.section.header,
        { type = "padding", val = 3 },
        opts.section.buttons,
        { type = "padding", val = 2 },
        opts.section.footer,
      }

      return opts
    end,
  },

  -- You can disable default plugins as follows:
  { "max397574/better-escape.nvim", enabled = false },

  -- You can also easily customize additional setup of plugins that is outside of the plugin's setup call
  {
    "L3MON4D3/LuaSnip",
    config = function(plugin, opts)
      require "astronvim.plugins.configs.luasnip"(plugin, opts) -- include the default astronvim config that calls the setup call
      -- add more custom luasnip configuration such as filetype extend or custom snippets
      local luasnip = require "luasnip"
      luasnip.filetype_extend("javascript", { "javascriptreact" })
      require("luasnip.loaders.from_vscode").lazy_load {
        paths = { vim.fn.stdpath "config" .. "/snippets" },
      }
    end,
  },

  -- {
  --
  --   "hrsh7th/nvim-cmp",
  --   config = function()
  --     local cmp = require("cmp")
  --     cmp.setup {
  --       mapping = {
  --         ["<CR>"] = cmp.mapping.confirm {
  --           behavior = cmp.ConfirmBehavior.Replace,
  --           select = true,
  --         },
  --       },
  --     }
  --   end,
  -- },

  -- tailwind-tools.lua
  {
    "luckasRanarison/tailwind-tools.nvim",
    name = "tailwind-tools",
    build = ":UpdateRemotePlugins",
    dependencies = {
      "nvim-treesitter/nvim-treesitter",
      "nvim-telescope/telescope.nvim", -- optional
      "neovim/nvim-lspconfig", -- optional
    },
    opts = {}, -- your configuration
  },

  {
    "windwp/nvim-autopairs",
    config = function(plugin, opts)
      require "astronvim.plugins.configs.nvim-autopairs"(plugin, opts) -- include the default astronvim config that calls the setup call
      -- add more custom autopairs configuration such as custom rules
      local npairs = require "nvim-autopairs"
      local Rule = require "nvim-autopairs.rule"
      local cond = require "nvim-autopairs.conds"
      npairs.add_rules(
        {
          Rule("$", "$", { "tex", "latex" })
            -- don't add a pair if the next character is %
            :with_pair(cond.not_after_regex "%%")
            -- don't add a pair if  the previous character is xxx
            :with_pair(
              cond.not_before_regex("xxx", 3)
            )
            -- don't move right when repeat character
            :with_move(cond.none())
            -- don't delete if the next character is xx
            :with_del(cond.not_after_regex "xx")
            -- disable adding a newline when you press <cr>
            :with_cr(cond.none()),
        },
        -- disable for .vim files, but it work for another filetypes
        Rule("a", "a", "-vim")
      )
    end,
  },

  {
    "rebelot/heirline.nvim",
    dependencies = { "Zeioth/heirline-components.nvim" },
    opts = function(_, opts) return opts end,
    config = function(_, opts)
      local heirline = require "heirline"
      local heirline_components = require "heirline-components.all"

      -- Setup
      heirline_components.init.subscribe_to_events()
      heirline.load_colors(heirline_components.hl.get_colors())
      heirline.setup(opts)
    end,
  },

  -- Add my plugins
  {
    "brneor/hop.nvim",
    version = "*",
    event = "BufRead",
    config = function()
      require("hop").setup {
        keys = "tnplvmdhsefucwyxriao",
        multi_windows = false,
      }
      vim.api.nvim_set_keymap("", "s", "<cmd>HopWord<cr>", { silent = true })
      vim.api.nvim_set_keymap("", "h", "<cmd>HopChar2<cr>", { silent = true })
      vim.api.nvim_set_keymap("", "L", "<cmd>HopLine<cr>", { silent = true })
      vim.api.nvim_set_keymap("", "l", "<cmd>HopLineStart<cr>", { silent = true })
      vim.api.nvim_set_keymap(
        "",
        "f",
        "<cmd>:lua require'hop'.hint_char1({direction = require'hop.hint'.HintDirection.AFTER_CURSOR,current_line_only = true})<cr>",
        { silent = true }
      )
      vim.api.nvim_set_keymap(
        "",
        "F",
        "<cmd>:lua require'hop'.hint_char1({direction = require'hop.hint'.HintDirection.BEFORE_CURSOR,current_line_only = true})<cr>",
        { silent = true }
      )
      vim.api.nvim_set_keymap("", "<C-w>", "<cmd>HopWordCurrentLineAC<cr>", { silent = true })
      vim.api.nvim_set_keymap("", "<C-b>", "<cmd>HopWordCurrentLineBC<cr>", { silent = true })
      vim.api.nvim_set_keymap(
        "",
        "t",
        "<cmd>:lua require'hop'.hint_char1({direction = require'hop.hint'.HintDirection.AFTER_CURSOR,current_line_only = true,hint_offset = -1})<cr>",
        { silent = true }
      )
      vim.api.nvim_set_keymap(
        "",
        "T",
        "<cmd>:lua require'hop'.hint_char1({direction = require'hop.hint'.HintDirection.BEFORE_CURSOR,current_line_only = true,hint_offset = 1})<cr>",
        { silent = true }
      )
    end,
  },

  -- Better jump to line
  {
    "nacro90/numb.nvim",
    event = "BufRead",
    config = function()
      require("numb").setup {
        show_numbers = true,
        show_cursorline = true,
      }
    end,
  },

  -- Preview markdown on neovim (requires Glow binary)
  { "ellisonleao/glow.nvim", config = true, cmd = "Glow" },

  -- Mappings to delete, change and add surroundings
  -- { 'tpope/vim-surround' },
  {
    "roobert/surround-ui.nvim",
    dependencies = {
      "kylechui/nvim-surround",
      "folke/which-key.nvim",
    },
    config = function()
      require("surround-ui").setup {
        root_key = "i",
      }
    end,
  },
  {
    "kylechui/nvim-surround",
    version = "*", -- Use for stability; omit to use `main` branch for the latest features
    config = function()
      require("nvim-surround").setup {
        -- Configuration here, or leave empty to use defaults
      }
    end,
  },

  -- Calls lazygit from Neovim (requires lazygit binary)
  { "kdheepak/lazygit.nvim" },

  -- Search and replace
  {
    "windwp/nvim-spectre",
    event = "BufRead",
    -- TODO: melhorar os keybindings desse plugin
    config = function() require("spectre").setup() end,
  },

  -- Scrollbars
  {
    "petertriho/nvim-scrollbar",
    config = function()
      require("scrollbar").setup {
        marks = {
          GitAdd = {
            text = ".",
            highlight = "GitSignsAdd",
          },
          GitChange = {
            text = ".",
            highlight = "GitSignsChange",
          },
        },
      }
      require("scrollbar.handlers.gitsigns").setup()
    end,
  },

  -- Better glance at matched information, with support to show on scrollbars.
  {
    "kevinhwang91/nvim-hlslens",
    config = function() require("scrollbar.handlers.search").setup() end,
  },

  -- Funny animations with buffer content
  { "Eandrju/cellular-automaton.nvim" },

  -- Use the w, e, b motions like a spider. Considers camelCase and skips insignificant punctuation
  {
    "chrisgrieser/nvim-spider",
    lazy = true,
    config = function()
      require("spider").setup {
        skipInsignificantPunctuation = false,
      }
    end,
  },

  -- Detect current indent
  { "tpope/vim-sleuth" },

  -- Prevent the cursor from moving when using shift and filter actions
  {
    "gbprod/stay-in-place.nvim",
    config = function() require("stay-in-place").setup() end,
  },

  -- bookmarks
  {
    "LintaoAmons/bookmarks.nvim",
    tag = "v0.5.4", -- optional, pin the plugin at specific version for stability
    dependencies = {
      { "nvim-telescope/telescope.nvim" },
      { "stevearc/dressing.nvim" }, -- optional: to have the same UI shown in the GIF
    },
  },

  -- better markdown test
  {
    "MeanderingProgrammer/markdown.nvim",
    main = "render-markdown",
    opts = {},
    name = "render-markdown", -- Only needed if you have another plugin named markdown.nvim
    dependencies = { "nvim-treesitter/nvim-treesitter", "echasnovski/mini.nvim" }, -- if you use the mini.nvim suite
  },

  -- copilot chat
  {
    "CopilotC-Nvim/CopilotChat.nvim",
    dependencies = {
      { "zbirenbaum/copilot.lua" },
      { "nvim-lua/plenary.nvim", branch = "master" },
    },
    build = "make tiktoken",
    opts = {
      -- See Configuration section for options
    },
  },
  -- neorg
  -- {
  --   "vhyrro/luarocks.nvim",
  --   priority = 1000,
  --   config = true,
  -- },
  -- {
  --   "nvim-neorg/neorg",
  --   dependencies = { "luarocks.nvim" },
  --   lazy = false, -- Disable lazy loading as some `lazy.nvim` distributions set `lazy = true` by default
  --   version = "*", -- Pin Neorg to the latest stable release
  --   config = function()
  --     require("neorg").setup({
  --       load = {
  --         ["core.defaults"] = {},
  --         ["core.concealer"] = {},
  --       }
  --     })
  --   end,
  -- }
}
